from conan import ConanFile

class MksMicrovisionIpRgaRecipe(ConanFile):
    name = "mks_microvisionip_rga"
    executable = "ds_MKS_MicrovisionIP_RGA"
    version = "3.1.7"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Jean Coquet"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/vacuum/mks_microvisionip_rga.git"
    description = "MksMicrovisionIpRga device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("socket/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
